#coding:utf-8
__author__ = 'RuanL'

#opencv RGB存储顺序为B,G,R
#坐标为矩阵坐标m，n，与x，y不一致
#opencv读取时会将竖图旋转成横图
#需要注意

import sqlite3
import cv2

# 初始化
name = '02'
origin_img = cv2.imread("origin_data/" + name + ".jpg")


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get_site(p1, p2, rate1, rate2):
    site = {'x':(rate1*p1['x']+rate2*p2['x'])/(rate1+rate2) , 'y':(rate1*p1['y']+rate2*p2['y'])/(rate1+rate2)}
    return site

def get_colors(pic_name):
    name = pic_name

    # 连接数据库
    conn = sqlite3.connect("data_info.db")
    conn.row_factory = dict_factory

    # 操作数据库获得数据
    curs = conn.cursor()
    res = curs.execute("select * from location where pic_name = '%s'" % name)
    location = res.fetchone()

    # 二者坐标相差90度,初始化一些位置信息
    pa = {'y':location['ax'],'x':location['ay']}
    pb = {'y':location['bx'],'x':location['by']}
    pc = {'y':location['cx'],'x':location['cy']}
    pd = {'y':location['dx'],'x':location['dy']}

    w1 = abs(pd['y'] - pa['y'])
    w3 = abs(pc['y'] - pb['y'])
    w2 = (w1 + w3)/2

    h1 = abs(pb['x'] - pa['x'])
    h3 = abs(pc['x'] - pd['x'])
    h2 = (h1 + h3)/2

    site_a = get_site(pa,pb,3,1)
    site_b = get_site(pa,pb,1,3)
    site_c = get_site(pc,pd,3,1)
    site_d = get_site(pc,pd,1,3)

    colors = []

    print w1,w3,h1,h3
    sample_range = min(7 , int(min([w1,w3,h1,h3])/8))
    print sample_range

    # 获得每个色块中心点，并计算平均颜色
    for i in range(0,10):
        p = get_site(site_a,site_d,19-2*i,2*i+1)
        print p
    #    print(site_a,site_b,site_c,site_d)
        square = origin_img [p['x']-sample_range:p['x']+sample_range+1 , p['y']-sample_range:p['y']+sample_range+1]
        # cv2.namedWindow("ii")
        # cv2.imshow("ii",square)
        # cv2.imwrite("gg" + i.__str__()+".jpg",square)
        colors.append([int(square[...,0].mean()),int(square[...,1].mean()),int(square[...,2].mean())])
        p = get_site(site_b,site_c,19-2*i,2*i+1)
        print p,i
        square = origin_img [p['x']-sample_range:p['x']+sample_range+1 , p['y']-sample_range:p['y']+sample_range+1]
        cv2.namedWindow("ii")
        cv2.imshow("ii",square)
        cv2.imwrite("gg" + i.__str__()+".jpg",square)
        colors.append([int(square[...,0].mean()),int(square[...,1].mean()),int(square[...,2].mean())])

    # 将所有平均颜色存入数据库
    if  curs.execute("select * from colors where name = '%s'" % name).fetchall() == []:
        curs.execute("insert into colors (name) values ('%s')" % name)
        conn.commit()

    for j in  range(1,21):
        curs.execute("update colors set R_%s = %d,G_%s = %d,B_%s = %d  where name = '%s'" %(j.__str__(),colors[j-1][2],j.__str__(),colors[j-1][1],j.__str__(),colors[j-1][0],name))
    conn.commit()
    return colors
