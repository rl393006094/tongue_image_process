#coding:utf-8
__author__ = 'RuanL'

import cv2
import time

start = time.time()
sp = 64
sr = 20
name = "00"
# image read
origin_img = cv2.imread("origin_data/" + name + ".jpg")
# meanshift filtering
dst_img = cv2.pyrMeanShiftFiltering(origin_img,sp,sr)

# show image
cv2.imwrite("meanshift_data/" + name + "-" + sp.__str__()+"-"+sr.__str__()+".jpg",dst_img)
cv2.waitKey (0)

end = time.time()
print end - start
